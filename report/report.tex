% !TeX spellcheck = en_US
% arara: pdflatex
\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{lmodern,textcomp,amsmath}
\usepackage[T1]{fontenc}
\usepackage[english]{babel,varioref}

\usepackage[margin=2.5cm]{geometry}
\usepackage{parskip,microtype,layaureo}
\usepackage{xcolor,graphicx,subfig,float,tabularx,booktabs,multicol}
\usepackage{hyperref}
\usepackage{listings}
\definecolor{dkpowder}{rgb}{0,0.2,0.7}
\hypersetup{%
    pdfpagemode  = {UseOutlines},
    bookmarksopen,
    pdfstartview = {FitH},
    colorlinks,
    linkcolor = {black},
    citecolor = {black},
    urlcolor  = {dkpowder},
    %
    pdfauthor   = {Elia Bonetto},
    pdftitle    = {Project - Computer Vision},
    pdfsubject  = {},
    pdfkeywords = {},
    pdfproducer = {LaTeX},
    pdfcreator  = {pdfLaTeX}
}
\frenchspacing

\begin{document}

\captionsetup[subfloat]{labelformat=empty}

\begin{center}\Large
    \textbf{Vehicle and lane detection. Collision warning.}\\[1em]
    \textit{Elia Bonetto} -- \texttt{\large 1162800}\\[2em]
\end{center}

\paragraph*{Requirements}

(previously installed, plenty of guides online): 
\begin{itemize}
\item OpenCV - C++ {[}mandatory because of the course{]}
\begin{itemize}
	\item $3.4.2$ for the OpenCV project
	\item $<=3.4.1$ for darknet, there's a bug with $3.4.2$
\end{itemize}
\item Darknet
\item YOLOv3
\item Cityscapes dataset: annotations and leftImg8bit
\item Python3 - needed for a recursive flag with glob function
\item Ubuntu 18.04 / Bash on Windows 10
\item CUDA, eventually cuDNN
\end{itemize}
To install CUDA, Darknet, Tensorflow, OpenCV follow: 

\url{https://github.com/heethesh/Install-TensorFlow-OpenCV-GPU-Ubuntu-17.10}

\hypertarget{first-step-vehicle-and-road-area-detection}{%
\paragraph{First Step: Vehicle and road area detection}}


  \emph{note: all the sources, the configuration files and the results are inside the YOLOv3 folder}

As first step I wanted to use a CNN to detect vehicles and road regions to get an insight on what I have on the image and possibly restrict the area where to search my lane since I do not have a constant pattern on my images and I can't rely on a fixed setup to get some useful data. I did not want absolute precision on the road area but only a region as I will be more interested in my lane despite of the road as a whole.

Initially my idea was to use YOLOv3 as network, also to exploit its integration with OpenCV 3.4, but anyway I have scanned the web for alternatives since YOLO, as it is, it would be useful only for the car detection. I have found a brand new project with the \textbf{\href{https://xingangpan.github.io/projects/CULane.html}{CULane
Dataset}} related to the \textbf{\href{https://github.com/XingangPan/SCNN}{Spatial CNN for
traffic lane detection}} but this network, contrary to YOLO, does not detect cars.

Finally I have opted to the \textbf{\href{https://www.cityscapes-dataset.com/}{CityScapes}} dataset that detect roads and cars with high precision along with other $28$ classes. CityScapes is mainly trained on \href{https://towardsdatascience.com/training-road-scene-segmentation-on-cityscapes-with-supervisely-tensorflow-and-unet-1232314781a8}{TensorFlow
and UNet} or \href{https://github.com/facebookresearch/Detectron}{Detectron passing
through COCO}, no reference to YOLO training for this dataset except for a very old \href{https://goo.gl/2eXBVQ}{google-groups} post without any reference and for a blog post for \href{http://uhurumkate.blogspot.com/2017/12/automatic-license-plate-recognition.html}{license plate recognitions}.

After downloading from CityScapes website \emph{gtFine\_trainvaltest} (241 MB) and \emph{leftImg8bit\_trainvaltest} (11 GB) I extracted them following this structure:

\begin{itemize}
\item
  {[}Main Folder{]}
\item
  Annotations(the gtFine archive)

  \begin{itemize}
  \item
    Train

    \begin{itemize}
    \item
      {[}subfolders{]}
    \end{itemize}
  \item
    Val

    \begin{itemize}
    \item
      {[}subfolders{]}
    \end{itemize}
  \end{itemize}
\item
  Train (images)

  \begin{itemize}
  \item
    {[}subfolders{]}
  \end{itemize}
\item
  Val (images)

  \begin{itemize}
  \item
    {[}subfolders{]}
  \end{itemize}
\end{itemize}

I haven't used the test part.

At this point the first step is to select the classes that are useful for my purposes. Since I do not have a really good hardware and for each class the suggestion is to run $2000$ iterations of a YOLO training for a first run I have decided to use only the \emph{road} and the \emph{car} ones.

To do that I have to create a \emph{.txt} file for each image, put it in the same directory and with the same name of the image(except for the extension), and put into that text file one line for each object containing:

\begin{itemize}
\item
  object-class integer object number from 0 to (classes-1), in our case
  0 for car and 1 for road
\item
  x: absolute\_x / image\_width
\item
  y: absolute\_y / image\_height
\item
  width: absolute\_width / image\_width
\item
  height: absolute\_height / image\_height
\end{itemize}

Note that x and y are the center of the object not the top-left corner.

The annotation files previously downloaded from the CityScapes dataset comes as \emph{.json} and the structure is pretty simple:

\begin{lstlisting}
{"imgHeight": 1024, 
"imgWidth": 2048, 
"objects": [
	{[
	"Label": "name of the class",
	"polygon": [array of coordinates]
	]},
	{...},
	...
]}
\end{lstlisting}

So I made a small program in Python3(\texttt{extract\_txt.py}) to automatize this process of extracting the information and create the txt.

After this all the images and txt files have to be moved to the same folders, one for training and one for validation. There are plenty of way to do this, a simple one would be
\texttt{mv\ /**/*.{[}ext{]}\ {[}destination{]}} and since it's necessary to create the list of files to be used on training and the one to be used on validation I used another python program \texttt{process.py}.

Inside Darknet folder where I will perform the training there has to be the two lists and create the \emph{cfg} folder two files:

\begin{itemize}
\item
  \emph{{[}name{]}.names}: the name of the classes:
\end{itemize}

\begin{lstlisting}[breaklines]
    car 
    road
\end{lstlisting}

\begin{itemize}
\item
  \emph{{[}name{]}.data} containing the number of classes, the location of the train and validation lists, the location of the \emph{names} file and the location where I want the output weights. In my case
\end{itemize}

\begin{lstlisting}[breaklines]
    classes= 2  
    train  = train.txt  
    valid  = test.txt  
    names = [name].names  
    backup = backup/
\end{lstlisting}

Where backup is the folder(that has to exist), where the computed weights will be putted.

For the YOLO configuration file I have opted for tiny-yolo again because of performance problems so I copied, always inside the \emph{cfg} folder \texttt{cp\ yolov3-tiny.cfg\ {[}name{]}.cfg} and modified this according to my configuration.

\begin{lstlisting}[breaklines]
Line 3: set batch=24, this means I will be using 24 images for every training step
Line 4: set subdivisions=8, the batch will be divided by 8 to decrease GPU VRAM requirements.
Line 127: set filters=(classes + 5)*3 in our case filters=21
Line 135: set classes=2, the number of categories we want to detect
Line 171: set filters=(classes + 5)*3 in our case filters=21
Line 177: set classes=2, the number of categories we want to detect
\end{lstlisting}

To perform the training I decided to use an initial set of weights mainly because nearly everybody online suggest to go this way. So I have downloaded default \href{https://pjreddie.com/media/files/yolov3-tiny.weights}{tiny
weights} and used \texttt{.\textbackslash{}darknet\ partial\ cfg/yolov3-tiny.cfg\ yolov3-tiny.weights\ yolov3-tiny.conv.15\ 15 } (where 15 is the number of layers) to get pre-trained weights.

The training is done with \texttt{.\textbackslash{}darknet\ detector\ train\ data/{[}name{]}.data\ {[}name{]}.cfg\ yolov3-tiny.conv.15}.

Following the common suggestion to train for at least 2000 iterations for each class I set the maximum number of possible iteration at 5100 to match this requirement and don't take too much time at the same time. Initially the results were very bad since the error went up and down around $400\%$. After $1.5k$ iterations this improves way better going around $6-9\%$ and when the iterations are more than $2.5/3k$ I have some flips between $4.3$ and $5.8$ as you can see from the graph.

At this point I have done some changes on the configuration file creating various alternatives to check if changing some parameters like the anchors would have improved my results.

\begin{itemize}
\item configuration 2: changed the anchors accordingly to \texttt{./darknet detector calc\_anchors data/[name].data -num\_of\_clusters 9 -width 416 -height 416} where width and height are the ones in the config file
\item configuration 3: doubled the width and height($832$) and set the $subdivisions=16$ because of GPU memory requirments
\item configuration 4: standard width and height($416$) and increase batch size to $64$ and subdivision to $16$
\end{itemize}

\begin{figure}[h!]
	\centering
	\subfloat[First configuration]{\includegraphics[width=0.45\textwidth]{../YOLOv3/cfg1/chart.jpg}}\quad
	\subfloat[Second configuration]{\includegraphics[width=0.45\textwidth]{../YOLOv3/cfg2/chart.jpg}}\\
	\subfloat[Third configuration]{\includegraphics[width=0.45\textwidth]{../YOLOv3/cfg3/chart.jpg}}\quad
	\subfloat[Fourth configuration]{\includegraphics[width=0.45\textwidth]{../YOLOv3/cfg4/chart.jpg}}\\
	\caption{The average loss for each iteration - Graph generated by \textit{Darknet}}
\end{figure}

It's easy to see how the first two configurations essentially works the same so in this case changing the anchors does not increase so much the precision. The third configuration works worser since it never goes under the $5\%$ average error rate. Increasing the size lead also to a sensible increasing of the processing time in my configuration of almost 2 times the previous one. The graph doesn't show that but in the first two cases after $1.5-2k$ iterations the average error was around $5-6\%$ while in the third one stays between $6$ and $7\%$. Finally the fourth configuration instead works a bit better and a bit earlier with respect to the the other tries.


To select the best weights is suggested to consider the higher average intersect of union(IoU) and mean average precision(mAP) values. The IoU is the ratio between the area of overlap and the area of union of the object's box and the detected box. The mAP is default metric of precision in the PascalVOC competition. This can be evaluated with \texttt{./darknet detector map [data] [config] [weights]}. In my case though this is not a good metric since the box of the road will drive the results because of its big area. I have opted to use one of the final weights of the fourth classifier in particular where it seems to have the lower average error, so around 4800 iterations. This is not a precise choice but it works pretty well. 

A marginal note has to be done on the dataset since it presents some errors in the definition of the labels. 
Some of them goes out of the picture(maximum pixel in the y direction for example comes up to be 2049 in a 2048 picture), causing the ratio of the box with respect to the image size considered to be above 1. 
This has been managed in the python script but since there are only few images that have this problem I have not restart the whole computations from the beginning after finding out.
For reference take as example the road definition of \textit{munster\_000035\_000019}.


\paragraph{Second step: C++ code and lane detection}
The main work is done by the classifier and the lane detection classes. The program takes in input the weights, the network configuration, the image, the confidence for the network and a percentage to check if the car is too near.
 
Initially I resize the image to have a common width alongside the whole program so smaller images becomes bigger and vice-versa, mantaining all the proportions since I use a percentage-based scaling.

The first step now, after checking the inputs and loading the network, is to detect cars using the trained network and create boxes to wrap the found cars. At this point there is a first check: if I have no car I obviously exit the program, if, 
on the other hand I have cars, and the biggest one occupy too much space on the image I know for sure that I am too near and so I stop my vehicle. If all up to now goes well I can search for my lane and apply the threshold on the distance which is a percentage threshold since I do not have a constant pattern on the images.

To find the lane the first thought was to apply a perspective transform to get a bird-view and to search the lane in a specific area of my picture and to use the parameters of the camera to get the distance of the founded cars to retrieve a result. The problem is that this method is useful only if I have knowledge of the camera and if the pictures have the field of view, the same focus and has been taken from the same position and this is not the case. So I ended up to use the Hough procedure to find the lines characterizing the lanes and use them to have a good area where to check the collision condition.
As first step I define a region of interest(ROI) but thanks to the variability of the images, I cannot use the one given by the detector since that leads to poor results and probably that's too wide with respect to a manually defined one.

Given this (pre-defined)region I mask the image and do all the operations inside, firstly smoothing with a bilateral filter to better preserve the edges and mask the yellow and white colors to get the lines. Eroding and dilating is then used to get rid of noise and points like reflections in the asphalt, I use different windows size since test show that this is better than a single closing operation that would have lead to some images to completely loose the thresholded parts.
A Sobel mask and a probability-Hough does the lines search that will be filtered based on the position and on the agle in left and right lines. Here I suppose that the car is in the center of the lane or at least that my lane has the left border on the left with respect to the middle of the image and the right is on the opposite side.
If no line is found I exit the procedure and I use the default ROI for getting the results, otherwise I go on and once for the left side once for the right one, filtering out some outliers, I fit a line using the couple of points given by \texttt{HoughLinesP}. In the end I update the ROI with the delimiting points I have found. Please note that this works even if the line is detected only on the left or in the right side of the image taking as base reference for the missing part the predefined points.


For the ROI just defined I search finally if I have cars inside and check if the threshold condition on the distance is satisfied or not. 

Regarding the structure of my program I have a class \texttt{boxes} that are simple rectangles extended by \texttt{detected\_boxes} where I store also the class of the box and the confidence given by the detector. Regarding the lane part I have done a \texttt{lane} class that extend \texttt{region} by adding the image where search the lane and overloading the draw method.
The boxes and region class are different since the boxes admit rectangles only while the region admits four points polygons.
The classifier is mainly based on the OpenCV code but there are some modifications to better fit my code and my necessities. 
\paragraph{Results and conclusions}
The program works well in my opinion on the given dataset and on a bunch of tested images of the Cityscapes dataset. 

Please note that the program does not require fine management for filtering the lines since it works as it is with most of the pictures without needing human interaction. Having a common camera frame and a common position could improve the results a lot because I could introduce a finer initial ROI and use the camera parameters to better work on the images and estimate distances.

The processing time is aroud $0.3$ seconds from the loading of the image to the drawing of the results: too slow for real-time computation, i.e. at 100 km/h this is around 8.3 m of space. The main part that takes from $0.2$ to $0.28$ is the classifier in particular the forward method on the network, nothing I can do about it.

The main problems on the results that I have obtained with the test images are on the lane detection part, sometimes due to my initial ROI wich is very wide and include several background in various cases or other lanes, cars and various elements that cannot be easily filtered out with colours or else.

Latter tests pointed out that probably the main problem is related to the thresholding operation to find the white lines. Reflections, poor light conditions, noise with other horizontal signs, greying of the white lane lines cause poor performance that cannot be predicted. Even with a different threshold only on the grey levels and an histogram equalization like the one done in the \texttt{grey} branch of the repository do not improve performance, at the end some pictures are better and others are worser.
 

Increasing the filtering on the angle of the edge would be possible with a dataset with constant characteristics as already pointed out.


The algorithm probably would benefit with an insertion of black boxes where the cars are located or increasing the ROI interatively until the lane is discovered. The last option is also a bit dangerous since in this case I do not have mean to say if the lanes I've found are the right one to estimate the correct region or are only some artifacts and in addition would be probably way slower in my case.

Finally the main implementation seems good enough since out of 12 images it detects correctly completely the lane on 7 over 10 tries[1, 2, 3, 4, 5, 9, personal 2], partially on 1 over 10 [6], and wrongly on the remaining 2[8 and personal 1]. For image 10 and 7, by tuning the ratio threshold about the width of the car in the image, one can see that in the former case the lane is correctly identified and in the latter is not but that is a very poor image very small with a lot of noise.


\begin{figure}[h!]
	\centering
	\subfloat[Start]{\includegraphics[width=0.3\textwidth]{workflow/i1.jpg}}\quad
	\subfloat[ROI]{\includegraphics[width=0.3\textwidth]{workflow/i2.jpg}}\quad
	\subfloat[Crop and blur]{\includegraphics[width=0.3\textwidth]{workflow/i3.jpg}}\\
	\subfloat[HSL]{\includegraphics[width=0.3\textwidth]{workflow/i4.jpg}}\quad
	\subfloat[Threshold]{\includegraphics[width=0.3\textwidth]{workflow/i5.jpg}}\quad
	\subfloat["Closing"]{\includegraphics[width=0.3\textwidth]{workflow/i6.jpg}}\\
	\subfloat[Sobel]{\includegraphics[width=0.3\textwidth]{workflow/i7.jpg}}\quad
	\subfloat[Lines]{\includegraphics[width=0.3\textwidth]{workflow/i8.jpg}}\quad
	\subfloat[Final ROI]{\includegraphics[width=0.3\textwidth]{workflow/i9.jpg}}\\
	\caption{The workflow, except for the YOLO-detection steps}
\end{figure}

\begin{figure}[h!]
	\centering
	\subfloat[image 1]{\includegraphics[width=0.3\textwidth]{results/i1.jpg}}\quad
	\subfloat[image 2]{\includegraphics[width=0.3\textwidth]{results/i2.jpg}}\quad
	\subfloat[image 3]{\includegraphics[width=0.3\textwidth]{results/i3.jpg}}\\
	\subfloat[image 4]{\includegraphics[width=0.3\textwidth]{results/i4.jpg}}\quad
	\subfloat[image 5]{\includegraphics[width=0.3\textwidth]{results/i5.jpg}}\quad
	\subfloat[image 6]{\includegraphics[width=0.3\textwidth]{results/i6.jpg}}\\
	\subfloat[image 7]{\includegraphics[width=0.3\textwidth]{results/i7.jpg}}\quad
	\subfloat[image 8]{\includegraphics[width=0.3\textwidth]{results/i8.jpg}}\quad
	\subfloat[image 9]{\includegraphics[width=0.3\textwidth]{results/i9.jpg}}\\
	\subfloat[image 10]{\includegraphics[width=0.3\textwidth]{results/i10.jpg}}\quad
	\subfloat[personal 1]{\includegraphics[width=0.3\textwidth]{results/pers1.jpg}}\quad
	\subfloat[personal 2]{\includegraphics[width=0.3\textwidth]{results/pers2.jpg}}\\
	\caption{The results, confidence and threshold $=0.3$}
\end{figure}
\end{document}
