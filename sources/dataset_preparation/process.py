#Place this file in the root folder where you have
#the validation and train subfolders containing the png images.
#This program will create the txt files containing the list of the
#images for the training and the validation partition

#Example:
#MYFOLDER
#|_process.py
#|_val
#|__images and labeling for validation
#|_train
#|__images and labeling for training


import glob, os

# Current directory
current_dir = os.path.dirname(os.path.abspath(__file__))


# Create and/or truncate train.txt and test.txt
file_train = open('train.txt', 'w')  
file_test = open('test.txt', 'w')

test = glob.glob('val/**.png')
for file in test:
	file_test.write(current_dir + "/" + file +"\n")
train = glob.glob('train/**.png')
for file in train:
	file_train.write(current_dir + "/"+ file + "\n")
