#include <opencv2/imgproc.hpp>
#include "region.h"

using namespace std;
using namespace cv;

/// <summary>
/// Create a region giving four points. More general of boxes since boxes are rect.
/// </summary>
/// <param name="t*">The point of one of the four angle of the region</param>
region::region(Point tl, Point tr, Point br, Point bl) {
    points.push_back(tl);
    points.push_back(tr);
    points.push_back(br);
    points.push_back(bl);
}

/// <summary>
/// Draw the region as contours. Different from lane draw function.
/// </summary>
/// <param name="img">The image where to draw</param>
void region::draw(Mat &img) {
    for (size_t i = 0; i < points.size(); i++)
        //cv_aa anti aliased
        line(img, points.at((i) % 4), points.at((i + 1) % 4), Scalar(255, 255, 255), 3, CV_AA);
}

/// <summary>
/// Update the points that give the region.
/// </summary>
/// <param name="t*">The new value of the points of the region</param>
void region::update(Point tl, Point tr, Point br, Point bl) {
    points.at(0) = tl;
    points.at(1) = tr;
    points.at(2) = br;
    points.at(3) = bl;
}

/// <summary>
/// Return the region.
/// </summary>
/// <returns>A vector of viable points enclosing the region</returns>
vector<Point> region::getRegion() const {
    return points;
}
