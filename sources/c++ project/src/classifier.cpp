#include "classifier.h"
#include <iomanip>
#include <iostream>

using namespace cv;
using namespace std;

/// <summary>
/// Returns the network names for the output layers
/// </summary>
/// <param name="net">The network where to get the Output Layer Name</param>
/// <returns>A vector of viable names</returns>
vector<String> getOutputsNames(const dnn::Net &net);

/// <summary>
/// Find the searched boxes and refine the results
/// </summary>
/// <param name="frame">The image where to search</param>
/// <param name="outs">Result of net.forward</param>
/// <param name="net">The network</param>
/// <param name="detected"> The list of detected boxes</param>
/// <param name="ptr_classifier">A pointer to the YOLOclassifier object to get the needed parameters</param>
void process(vector<detected_boxes> &detected, Mat &frame, const vector<Mat> &outs, dnn::Net &net,
             YOLOClassifier &ptr_classifier);

/// <summary>
/// Constructor for the YOLO classifier
/// </summary>
/// <param name="cfg_file">The yolo config file path</param>
/// <param name="weights_file">The trained yolo weights path</param>
/// <param name="confidence_threshold">The threshold to be used</param>
YOLOClassifier::YOLOClassifier(const String &cfg_file, const String &weights_file, const float confidence_threshold) :
        classes(vector<string>(2)), colors(vector<Scalar>(2)), threshold(confidence_threshold) {

    //use the hardcoded initializer and load the config and the weights for my network
    init();
    try {
        cout << "Model cfg: " << cfg_file << endl;
        cout << "Model weights: " << weights_file << endl;
        net = dnn::readNetFromDarknet(cfg_file, weights_file);
    }
    catch (Exception &e) {
        cout << "WARNING: YOLOClassifier not initialized correctly!" << endl;
        cout << e.what() << endl;
    }
}

/// <summary>
/// Initialize classes and colors relative to them. Hardcoded
/// </summary>
void YOLOClassifier::init() {
    classes[0] = "car";
    colors[0] = Scalar(0, 0, 255);
    classes[1] = "road";
    colors[1] = Scalar(0, 255, 0);
}

/// <summary>
/// Get the predefined classes.
/// </summary>
/// <returns>the classes</returns>
vector<string> YOLOClassifier::get_classes() const {
    return classes;
}

/// <summary>
/// Get the colors
/// </summary>
/// <returns>The colors</returns>
vector<Scalar> YOLOClassifier::get_colors() const {
    return colors;
}

/// <summary>
/// Get the threshold
/// </summary>
/// <returns>The threshold</returns>
float YOLOClassifier::get_threshold() const {
    return threshold;
}

/// <summary>
/// Draw the boxes in the passed image.
/// </summary>
void YOLOClassifier::draw(Mat &img, vector<detected_boxes> detectedBoxes) {

    //generate boxes and labels to be drawed in the image
    for (auto i:detectedBoxes) {
        Rect box = i.getBox();

        Scalar roi_color(colors.at(i.getClass()));

        //create the string cropping the confidence at 2 decimal number precision
        stringstream stream;
        stream << fixed << setprecision(2) << i.getConf();
        String label(classes.at(i.getClass()) + ":" + stream.str());

        Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, nullptr);

        rectangle(img, Point(box.x, box.y), Point(box.x + box.width, box.y + box.height), roi_color);
        rectangle(img, Point(box.x, box.y + box.height),
                  Point(box.x + labelSize.width, box.y + box.height + labelSize.height), Scalar::all(255), FILLED);

        putText(img, label, Point(box.x, box.y + box.height + labelSize.height), FONT_HERSHEY_SIMPLEX, 0.5,
                Scalar(0, 0, 0));

    }
}

/// <summary>
/// Classify the objects in the given image
/// </summary>
/// <param name="frame">the given image</param>
/// <returns>The Boxes of the found objects</returns>
vector<detected_boxes> YOLOClassifier::classify(const Mat &frame) {

    Mat result(frame.clone());


    //Creates 4-dimensional blob from image, set as network input and forward it to the network
    //blob from image subtract the mean rgb, apply a scale factor, resize to what the CNN expect
    //swap RB channels and don't crop

    //adapt wrt the chosen weights if needed (size of the blob)
    Mat blob = dnn::blobFromImage(result, 1 / 255.F, Size(416, 416), Scalar(), true, false);

    vector<Mat> outs;
    vector<detected_boxes> boxes;
    net.setInput(blob);
    net.forward(outs, getOutputsNames(net));
    //once forwarded process the result from the network and get the boxes
    process(boxes, result, outs, net, *this);

    return boxes;
}

vector<String> getOutputsNames(const dnn::Net &net) {
    static vector<String> names;
    if (names.empty()) {
        vector<int> outLayers = net.getUnconnectedOutLayers();
        vector<String> layersNames = net.getLayerNames();
        names.resize(outLayers.size());
        for (size_t i = 0; i < outLayers.size(); ++i)
            names[i] = layersNames[outLayers[i] - 1];
    }
    return names;
}


void process(vector<detected_boxes> &detected, Mat &frame, const vector<Mat> &outs, dnn::Net &net,
             YOLOClassifier &ptr_classifier) {

    static vector<int> outLayers = net.getUnconnectedOutLayers();
    static string outLayerType = net.getLayer(outLayers[0])->type;

    vector<int> indices;
    vector<int> classIds;
    vector<float> confidences;
    vector<Rect> boxes;

    //output layer for the YOLO-darknet network. Can be expanded with other if necessary to generalize.
    if (outLayerType == "Region") {
        for (size_t i = 0; i < outs.size(); ++i) {

            // Network produces output blob with a shape NxC where N is a number of
            // detected objects and C is a number of classes + 4 where the first 4
            // numbers are [center_x, center_y, width, height]
            float *data = (float *) outs[i].data;

            for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols) {
                Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
                Point classIdPoint;
                double confidence;
                //Finds the global minimum and maximum in an array.
                //I'm not interested in the minimum so nullptr
                minMaxLoc(scores, nullptr, &confidence, nullptr, &classIdPoint);
                if (confidence > ptr_classifier.get_threshold()) {
                    int centerX = (int) (data[0] * frame.cols);
                    int centerY = (int) (data[1] * frame.rows);
                    int width = (int) (data[2] * frame.cols);
                    int height = (int) (data[3] * frame.rows);
                    int left = centerX - width / 2;
                    int top = centerY - height / 2;

                    //save class boxes and confidences to do NMS
                    classIds.push_back(classIdPoint.x);
                    confidences.push_back((float) confidence);
                    boxes.emplace_back(left, top, width, height);
                }
            }
        }
        //do non-maxima suppression, get the list of indices of the wanted boxes
        //10 as nms-threshold seems to work well, keep only the top 11 indices
        dnn::NMSBoxes(boxes, confidences, ptr_classifier.get_threshold(), 10, indices, 1.f, 11);
        for (size_t i = 0; i < indices.size(); i++) {
            int idx = indices[i];
            Rect box = boxes[idx];
            //save only the good boxes
            detected.emplace_back(
                    detected_boxes(box.x, box.y, box.width, box.height, classIds.at(idx), confidences.at(idx)));
        }
    } else
        CV_Error(Error::StsNotImplemented, "Unknown output layer type: " + outLayerType);

}