// Project.cpp : Defines the entry point for the console application.
//
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include "classifier.h"
#include "lane.h"

using namespace std;
using namespace cv;

//the wanted parameters
static const char *params =
        "{ help        |       | print this message                    }"
        "{ cfg         |       | YOLO model configuration              }"
        "{ weights     |       | YOLO model weights                    }"
        "{ src         |       | image folder path				       }"
        "{ conf        | 0     | YOLO min confidence(btw 0-1)          }"
        "{ thre        | 0	   | min allowed distance(btw 0-1)         }";

/// <summary>
/// Draw in the image if the car has to stop.
/// </summary>
/// <param name="img">The image where to write the alarm text</param>
/// <param name="ratio">The reason of the alarm, if ratio test(too wide) or if too close</param>
void stop(Mat img, bool ratio) {
    String text("STOP THE CAR!");
    ratio ? text += "RATIO TEST ALARM!" : text += "DISTANCE TEST ALARM!";

    putText(img, text, cvPoint(50, img.size().height / 2), FONT_HERSHEY_COMPLEX, 0.8, Scalar(0, 0, 255));
    imshow("Collision warning", img);
    waitKey(0);
    destroyAllWindows();
}

int main(int argc, char **argv) {
    CommandLineParser parser(argc, argv, params);

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    // reading parameters from the command line
    if (!(parser.has("cfg") && parser.has("weights") && parser.has("src"))) {
        cout << "FAILED TO READ MANDATORY CMD LINE ARGS!";
        cout << " you should specify the yolo config(-cfg) and yolo_weights(-weights) and image(-src) cmd line args!";
        cin.ignore();
        return 1;
    }

    //state standard parameters for not-mandatory ones
    if (!(parser.has("conf")))
        cout << "Standard confidence(0) has been taken for YOLO";
    if (!(parser.has("thre")))
        cout << "Standard distance threshold(0) has been taken for the car warning system";

    //create variables from command line parameters
    String cfg = parser.get<String>("cfg");
    String weights = parser.get<String>("weights");
    String path = parser.get<String>("src");

    float conf;
    parser.has("conf") ? conf = parser.get<float>("conf") : conf = 0;
    if (conf < 0 || conf > 1) {
        cout << "Confidence for yolo must be btw 0 and 1, setting 0 for this run" << endl;
        conf = 0;
    }

    double thre;
    parser.has("thre") ? thre = parser.get<double>("thre") : thre = 0;
    if (thre < 0 || thre > 1) {
        cout << "Distance threshold must be btw 0 and 1, setting 0 for this run" << endl;
        thre = 0;
    }
    //create the classifier
    YOLOClassifier classifier(cfg, weights, conf);

    Mat img = imread(path);

    if (img.empty()) {
        cout << "Cannot load/find the img. Exiting.." << endl;
        cin.ignore();
        return -1;
    }

    cout << "We have two classes: \n";
    cout << classifier.get_classes().at(0) << " of color " << classifier.get_colors().at(0) << "\n";
    cout << classifier.get_classes().at(1) << " of color " << classifier.get_colors().at(1) << endl;

    //resize the image to have a constant size where to work
    if (img.size().width != 1024) {
        double percentage = 1024.0 / img.size().width;
        resize(img, img, Size(0, 0), percentage, percentage);
    }

    //get the boxes from the classifier
    vector<detected_boxes> boxes = classifier.classify(img);

    //no boxes found nothing to check with
    if (boxes.empty()) {
        cout << "No car nor road found. Exiting.." << endl;
        imshow("Nothing", img);
        waitKey(0);
        cin.ignore();
        return 0;
    }

    int max_w(0);
    double ratio(0);
    region roi(Point(-1, -1), Point(0, 0), Point(0, 0), Point(0, 0));
    for (auto i:boxes) {
        if (i.getClass() == 0) {
            //update the width if inside the box I have a bigger car
            if (i.getBox().width > max_w) {
                max_w = i.getBox().width;

                //check the ratio threshold only if I update the max_w variable
                ratio = (double) max_w / img.size().width;
                if (ratio > 0.3) {
                    classifier.draw(img, boxes);
                    stop(img, true);
                    return 0;
                }
            }
        } else
            //if I found a road I get that as ROI
            roi = region(Point(i.getBox().x, i.getBox().y), Point(i.getBox().x + i.getBox().width, i.getBox().y),
                         Point(i.getBox().x + i.getBox().width, i.getBox().y + i.getBox().height),
                         Point(i.getBox().x, i.getBox().y + i.getBox().height));
    }


    //ratio == 0 only if max_w == 0 so no car found
    if (ratio == 0) {
        cout << "No car found. Exiting.." << endl;
        classifier.draw(img, boxes);
        imshow("No car", img);
        waitKey(0);
        cin.ignore();
        return 0;
    }

    //not working too well with the roi given by the road region
    if (roi.getRegion().at(0) == Point(-1, -1))
    roi.update(Point(img.size().width / 2 - 200, img.size().height / 2 - 50),
               Point(img.size().width / 2 + 200, img.size().height / 2 - 50),
               Point(img.size().width, img.size().height - 50),
               Point(0, img.size().height - 50));
    else
        roi.update(Point(img.size().width / 2 - 200, max(img.size().height / 2 - 50, roi.getRegion().at(0).y)),
                   Point(img.size().width / 2 + 200, max(img.size().height / 2 - 50, roi.getRegion().at(0).y)),
                   Point(img.size().width, img.size().height - 50),
                   Point(0, img.size().height - 50));

    //create lane obj
    lane lane(roi.getRegion().at(0), roi.getRegion().at(1), roi.getRegion().at(2), roi.getRegion().at(3), img);

    //detect lane
    lane.detect_lane_hough();


    //find cars inside ROI and check distance to the camera
    //get the lane ROI
    Mat lane_roi = Mat::zeros(img.size(), CV_8U);
    Point point_array[4];
    point_array[0] = lane.getRegion()[0];
    point_array[1] = lane.getRegion()[1];
    point_array[2] = lane.getRegion()[2];
    point_array[3] = lane.getRegion()[3];

    //fill the poly to check the intersection of the areas
    fillConvexPoly(lane_roi, point_array, 4, Scalar(255, 255, 255));
    Mat temp = Mat::zeros(lane_roi.size(), lane_roi.type());
    double min_distance = img.size().height;
    for (auto i:boxes) {
        if (i.getClass() == 0) {
            //foreach car get the ROI of the box
            Mat box_roi = Mat::zeros(img.size(), CV_8U);
            point_array[0] = Point(i.getBox().x, i.getBox().y);
            point_array[1] = Point(i.getBox().x + i.getBox().width, i.getBox().y);
            point_array[2] = Point(i.getBox().x + i.getBox().width, i.getBox().y + i.getBox().height);
            point_array[3] = Point(i.getBox().x, i.getBox().y + i.getBox().height);

            fillConvexPoly(box_roi, point_array, 4, Scalar(255, 255, 255));

            //get the and btw the box and lane ROIs
            bitwise_and(box_roi, lane_roi, temp);

            //if temp is all zeros then the car is out
            if (countNonZero(temp)) {
                //if distance from the bottom is lower than minimum so I have a nearer car I update
                if ((img.size().height - point_array[2].y) < min_distance) {
                    min_distance = img.size().height - point_array[2].y;

                    //I get the percentage of this min distance with respect to the height of the picture
                    //I check here so I can stop at the first time I found the condition to stop
                    ratio = min_distance / img.size().height;
                    if (ratio < thre) {
                        lane.draw(img);
                        classifier.draw(img, boxes);

                        stop(img, false);
                        return 0;
                    }
                }
            }
        }
    }
    //if nothing happens and I have car I'm good to go
    lane.draw(img);
    classifier.draw(img, boxes);

    imshow("Everything good", img);
    waitKey(0);
    destroyAllWindows();
    return 0;
}

