#include <opencv2/imgproc.hpp>
#include <iostream>
#include "lane.h"

using namespace cv;
using namespace std;

/// <summary>
/// Constructor using region default one adding only the image where I'll do the operation
/// </summary>
/// <param name="t*">The point of the ROI for the lane l = left r = right etc</param>
/// <param name="img">The image where to search the lane</param>
lane::lane(Point tl, Point tr, Point br, Point bl, Mat img) : region(tl, tr, br, bl) {
    image = img;
}

/// <summary>
/// The whole detecting work for the lines.
/// Starting from a filter(to remove the noise), detecting the yellow/white lines
/// and updating the region if needed. If at the end the region is changed I use that for the car distance threshold
/// otherwise I have to use the default one. Even partial update of the region is admitted.
/// </summary>
void lane::detect_lane_hough() {
    //remove noise with bilateral - better for edge
    Mat temp;
    bilateralFilter(image, temp, 5, 200, 200);

    //apply ROI
    Mat mask = Mat::zeros(temp.size(), temp.type());
    Point point_array[4];
    copy(points.begin(), points.end(), point_array);
    fillConvexPoly(mask, point_array, 4, Scalar(255, 255, 255));
    bitwise_and(temp, mask, temp);

    //edge detector -  HSV yellow good but no white so HLS
    cvtColor(temp, temp, COLOR_BGR2HLS);

    Mat y_thre, w_thre;
    inRange(temp, Scalar(10, 0, 100), Scalar(30, 255, 255), y_thre);
    inRange(temp, Scalar(2, 135, 2), Scalar(255, 255, 255), w_thre);

    //merge the masks
    bitwise_or(y_thre, w_thre, mask);

    //erode and dilate to clean the image (similar to closing but different kernel sizes)
    erode(mask, mask, getStructuringElement(MORPH_RECT, Size(2, 2)), Point(-1, -1), 1);
    dilate(mask, mask, getStructuringElement(MORPH_RECT, Size(4, 4)), Point(-1, -1), 1);

    // Filter the binary image to obtain the edges
    Mat grad_x, grad_y, grad;
    Mat abs_grad_x, abs_grad_y;

    //ddepth = -1 to have the same depth of the input image
    //std kernel size 3
    Sobel(mask, grad_x, -1, 1, 0, 3, 1);
    Sobel(mask, grad_y, -1, 0, 1, 3, 1);
    convertScaleAbs(grad_x, abs_grad_x);
    convertScaleAbs(grad_y, abs_grad_y);

    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

    //find lines with Hough Lines Probability
    vector<Vec4i> line, right, left;
    HoughLinesP(grad, line, 1, CV_PI / 180, 30, 30, 30);

    //if I don't find line return. The ROI is not changed so I rely on the manual one or in the detected one.
    if (line.empty()) {
        cout << "No lines found. Cannot compute new ROI" << endl;
        return;
    }
    //draw the lines in grad matrix
    /*
    for (size_t i = 0; i < line.size(); i++) {
        Vec4i l = line[i];
        cv::line(grad, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 255, 255), 3, CV_AA);
    }
    */

    //given the line angle filter out the useless ones, add some tolerance in left/right part of the image
    double avg_l = 0;
    double avg_r = 0;
    for (auto i : line) {
        double angle = ((double) (i[3]) - (double) (i[1])) /
                       ((double) (i[2]) - (double) (i[0]));

        if (abs(angle) > 0.3 && abs(angle) < 5) {
            if (angle > 0 && max(i[2], i[0]) >= image.size().width / 2.0 * 0.95) {
                right.push_back(i);
                avg_r += min(i[2], i[0]);
            } else if (angle < 0 && min(i[0], i[2]) <= image.size().width / 2.0 * 1.05) {
                left.push_back(i);
                avg_l += min(i[2], i[0]);
            }
        }
    }

    Point br(0, 0), bl(0, 0), tr(0, 0), tl(0, 0);

    //fit the lines and extract the four points
    if (!left.empty()) {
        Vec4d fitted_l;
        avg_l /= left.size();

        vector<Point> pts_fit;
        for (size_t i = 0; i < left.size(); i++) {
            Vec4i l = left[i];
            if (min(l[0], l[2]) > avg_l ||
                left.size() < 4) {
                pts_fit.emplace_back(l[0], l[1]);
                pts_fit.emplace_back(l[2], l[3]);
            }
        }
        //if I don't have point's to fit I do not update tl and bl so they stay (0,0)
        if (!pts_fit.empty()) {
            //fit the a line using the points using an L12 distance function
            //optimal value not needed. Accuracies tested seems to work.
            fitLine(pts_fit, fitted_l, CV_DIST_L12, 0, 0.01, 0.01);

            bl = Point((int) ceil(fitted_l[2] + (image.size().height - fitted_l[3]) / fitted_l[1] * fitted_l[0]),
                       image.size().height);
            tl = Point((int) ceil(fitted_l[2]), (int) ceil(fitted_l[3]));
        }
    }

    //if I haven't updated tl.x (0 is impossible since it's top) I grap the pre-defined points
    if (tl.x == 0) {
        bl = points.at(3);
        tl = points.at(0);
    }


    if (!right.empty()) {
        Vec4d fitted_r;
        avg_r /= right.size();

        vector<Point> pts_fit;
        for (size_t i = 0; i < right.size(); i++) {
            Vec4i r = right[i];
            if (min(r[0], r[2]) < avg_r || right.size() < 4) {
                pts_fit.emplace_back(r[0], r[1]);
                pts_fit.emplace_back(r[2], r[3]);
            }
        }
        if (!pts_fit.empty()) {
            fitLine(pts_fit, fitted_r, CV_DIST_L12, 0, 0.01, 0.01);
            br = Point((int) ceil(fitted_r[2] + (image.size().height - fitted_r[3]) / fitted_r[1] * fitted_r[0]),
                       image.size().height);
            tr = Point((int) ceil(fitted_r[2]), (int) ceil(fitted_r[3]));
        }
    }
    if (br.x == 0) {
        br = points.at(2);
        tr = points.at(1);
    }

    //at this point I have chances that I have changed the ROI so I update that
    update(tl, tr, br, bl);
}

/// <summary>
/// Draw the lane as an overlay in the image passed.
/// Need to do with addWeighted since not all pics support the alpha channel.
/// </summary>
/// <param name="img">The image where to draw</param>
void lane::draw(Mat &img) {
    vector<vector<Point>> pts{points};
    Mat temp = Mat::zeros(img.size(), img.type());

    //create a polygon with a magenta color
    fillPoly(temp, pts, Scalar(255, 0, 255));
    double alpha = 0.6;

    //sum with weights the images since not all format(JPG) support colors with transparency.
    addWeighted(img, 1, temp, alpha, 0.1, img);
}