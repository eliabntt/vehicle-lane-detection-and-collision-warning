#include "boxes.h"

/// <summary>
/// The detected boxes to be used in the classifier. Derived from boxes. Uses it's default constructor adding class and confidence
/// </summary>
/// <param name="left">The x-coord of the top-left corner</param>
/// <param name="top">The y-coord of the top-left corner</param>
/// <param name="width">The width of the box</param>
/// <param name="height">The height of the box</param>
/// <param name="cls">The class of the object enclosed in the box</param>
/// <param name="conf">The confidence of the detection for the box</param>
detected_boxes::detected_boxes(int left, int top, int width, int height, int cls, double conf)
        : boxes(left, top, width, height) {
    classification = cls;
    confidence = conf;
}

/// <summary>
/// Get the class of the detected box
/// </summary>
/// <returns>the class index as int</returns>
int detected_boxes::getClass() const { return classification; }

/// <summary>
/// Get the confidence of the detected box
/// </summary>
/// <returns>the confidence as double</returns>
double detected_boxes::getConf() const { return confidence; }
