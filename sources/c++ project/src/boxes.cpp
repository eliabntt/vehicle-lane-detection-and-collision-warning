#include "boxes.h"

using namespace cv;

/// <summary>
/// Default constructor. Create a rect given the parameters.
/// </summary>
/// <param name="left">The x-coord of the top-left corner</param>
/// <param name="top">The y-coord of the top-left corner</param>
/// <param name="width">The width of the box</param>
/// <param name="height">The height of the box</param>
boxes::boxes(int left, int top, int width, int height) {
    box = Rect(left, top, width, height);
}

/// <summary>
/// Return the Rect that is the box.
/// </summary>
/// <returns>A Rect representing the box</returns>
Rect boxes::getBox() {
    return box;
}