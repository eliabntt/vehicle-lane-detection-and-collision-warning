#ifndef PROJECT_LANE_H
#define PROJECT_LANE_H

#include <opencv2/core.hpp>
#include "region.h"

class lane : public region {
public:
    lane(cv::Point tl, cv::Point tr, cv::Point br, cv::Point bl, cv::Mat img);

    void detect_lane_hough();

    void draw(cv::Mat &img);

protected:
    cv::Mat image;
};


#endif //PROJECT_LANE_H
