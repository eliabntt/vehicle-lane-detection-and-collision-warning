#ifndef PROJECT_BOXES_H
#define PROJECT_BOXES_H

#include <opencv2/imgproc.hpp>

class boxes {
public:
    boxes(int left, int top, int width, int height);
    cv::Rect getBox();

protected:
    cv::Rect box;
};

class detected_boxes : public boxes{
public:
    detected_boxes(int left, int top, int width, int height, int cls, double conf);
    int getClass() const;
    double getConf() const;

protected:
    int classification;
    double confidence;
};

#endif //PROJECT_BOXES_H
