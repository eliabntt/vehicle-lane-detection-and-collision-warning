#ifndef PROJECT_REGION_H
#define PROJECT_REGION_H

#include <opencv2/core.hpp>

class region {
public:
    region(cv::Point tl, cv::Point tr, cv::Point br, cv::Point bl);

    std::vector<cv::Point> getRegion() const;

    virtual void draw(cv::Mat &img);

    void update(cv::Point tl, cv::Point tr, cv::Point br, cv::Point bl);


protected:
    std::vector<cv::Point> points;
};

#endif //PROJECT_REGION_H
