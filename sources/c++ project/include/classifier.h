#ifndef PROJECT__CLASSIFIER___H
#define PROJECT__CLASSIFIER___H


#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/dnn.hpp>
#include "boxes.h"

class YOLOClassifier {
public:
    YOLOClassifier(const cv::String &cfg_file, const cv::String &weights_file, float confidence_threshold);

    std::vector<detected_boxes> classify(const cv::Mat &frame);

    void draw(cv::Mat &img, std::vector<detected_boxes> detectedBoxes);

    float get_threshold() const;

	std::vector<cv::Scalar> get_colors() const;

    std::vector<std::string> get_classes() const;

protected:
    void init();

private:
    std::vector<std::string> classes;
    std::vector<cv::Scalar> colors;
    cv::dnn::Net net;
    const float threshold;
};

#endif // PROJECT__CLASSIFIER___H