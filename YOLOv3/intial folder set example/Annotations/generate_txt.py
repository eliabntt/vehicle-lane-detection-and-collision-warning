#To be used with Python3 necessary for the recursive of glob.glob
#Place in the parent folder of the annotations

#after running the program you MUST move all the txt to the same location of
#the pictures. You can do that with mv ./val/**/*.txt [destination]

from __future__ import division
import json
import numpy as np
import glob

list = glob.glob('**/*.json', recursive=True)
for file in list:

	with open(file, 'r') as infile:
		data = json.load(infile)

	#recreate the file if exist and append
	#the file is called with the suffix leftImg8bit, corresponding to the images
	#and it's created at the same location of the json
	text_file = open(file[:-20]+'leftImg8bit.txt', "w")
	#text_file = open(file[:-20]+'leftImg8bit.txt', "a")

	imgHeight = data['imgHeight']
	imgWidth = data['imgWidth']
	data = data['objects']
	for f in data:
		if f['label'] == 'car':
			s= np.asarray(f['polygon'])
			xmax = s[:,0].max()
			xmin = s[:,0].min()
			ymax = s[:,1].max()
			ymin = s[:,1].min()
			x = 0.5*(xmax+xmin)/imgWidth
			y = 0.5*(ymax+ymin)/imgHeight
			height = (ymax-ymin)/imgHeight
			width = (xmax-xmin)/imgWidth
			text_file.write('0 %f %f %f %f \n' %(x, y, width, height))
		elif f['label'] == 'road':
			s= np.asarray(f['polygon'])
			xmax = s[:,0].max()
			xmin = s[:,0].min()
			ymax = s[:,1].max()
			ymin = s[:,1].min()
			x = 0.5*(xmax+xmin)/imgWidth
			y = 0.5*(ymax+ymin)/imgHeight
			height = (ymax-ymin)/imgHeight
			width = (xmax-xmin)/imgWidth
			text_file.write('1 %f %f %f %f \n' %(x, y, width, height))
	text_file.close()
	
	#if file is empty delete
	if os.stat(file[:-20]+'leftImg8bit.txt').st_size==0:
		os.remove(file[:-20]+'leftImg8bit.txt')
	
	
